package com.trytech.mongoocrawler.client.common.queue;


import java.util.UUID;

/**
 * fetcher事件处理器
 */
public abstract class FetcherWorkHandler extends CrawlerWorkHandler {
    protected DisruptorContext disruptorContext;
    private String uuid = UUID.randomUUID().toString();

    public FetcherWorkHandler(DisruptorContext disruptorContext){
        this.disruptorContext = disruptorContext;
    }
    public DisruptorContext getDisruptorContext() {
        return disruptorContext;
    }

    public void setDisruptorContext(DisruptorContext disruptorContext) {
        this.disruptorContext = disruptorContext;
    }

    @Override
    public String getHashKey() {
        return HASH_PREFIX+uuid;
    }
}

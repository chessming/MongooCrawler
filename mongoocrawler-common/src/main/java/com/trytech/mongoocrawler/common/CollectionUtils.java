package com.trytech.mongoocrawler.common;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月10日
 */
public class CollectionUtils {

    public static List newLinkedList(){
        return new LinkedList();
    }

    public static List newArrayList(){
        return new ArrayList();
    }
}

package com.trytech.mongoocrawler.common.protocol;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月22日
 */
interface Protocol {
    /***
     * 协议魔数32位
     */
    byte[] MAGIC = {(byte) 0xDA, (byte) 0xB5, (byte) 0xBF, (byte) 0xFA};
    /***
     * 每次读取的字节数
     */
    int readCount = 0;
    /***
     * 内容长度占多少位, 最长32位，即4096m
     */
    int CONTENT_BYTE_LENGTH = 4;

    /***
     * 传输数据的类型, 共1个字节，8位:0,url; 1,command; 2,data; 3,
     */
    int TYPE_BYTE_LENGTH = 1;
}

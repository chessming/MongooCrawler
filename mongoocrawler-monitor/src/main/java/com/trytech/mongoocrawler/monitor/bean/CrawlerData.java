package com.trytech.mongoocrawler.monitor.bean;

import lombok.Data;

/**
 * Created by coliza on 2018/5/24.
 */
@Data
public class CrawlerData {
    private int mode;
    private String modeLabel;
    private int crawlerCount;
    private String runStatus;
}

package com.trytech.mongoocrawler.monitor.bean;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by coliza on 2018/5/24.
 */
public class MonitorData {
    //爬虫信息
    private static CrawlerData crawlerData;
    //分布式模式下的客户端信息
    private static List<ClientInfo> clientDataList;

    static {
        crawlerData = new CrawlerData();
        clientDataList = new LinkedList();
    }

    public static void registerClient(ClientInfo clientInfo) {
        clientDataList.add(clientInfo);
    }

    public static void removeClient(final String clientIP) {
        clientDataList = clientDataList.stream().filter(clientInfo -> clientInfo.getIp().equals(clientIP)).collect(Collectors
                .toList());
    }

    public void setMode(int mode) {
        crawlerData.setMode(mode);
    }

    public void setModeLabel(String modeLabel) {
        crawlerData.setModeLabel(modeLabel);
    }

    public void setCrawlerCount(int crawlerCount) {
        crawlerData.setCrawlerCount(crawlerCount);
    }

    public void setRunStatus(String runStatus) {
        crawlerData.setRunStatus(runStatus);
    }

    public CrawlerData getServerConfig() {
        if (crawlerData == null) {
            return new CrawlerData();
        }
        return crawlerData;
    }

    public List<ClientInfo> getClientsConfig() {
        return clientDataList;
    }


}

package com.trytech.mongoocrawler.monitor.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月01日
 */
public class DateUtil {
    private static SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String getNow() {
        return sf.format(new Date());
    }
}

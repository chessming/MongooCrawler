package com.trytech.mongoocrawler.server;

import com.trytech.mongoocrawler.server.common.exception.DataSourceInitException;
import com.trytech.mongoocrawler.server.xml.XmlConfigBean;
import com.trytech.mongoocrawler.server.xml.XmlDocumentBuilder;
import org.dom4j.DocumentException;
import org.xml.sax.SAXException;

import java.io.IOException;

/**
 * Created by hp on 2017-2-17.
 */
public class CrawlerConfig {
    private static XmlConfigBean configBean;

    public static CrawlerConfig newInstance(String path) throws IOException, DocumentException, SAXException, DataSourceInitException {
        XmlDocumentBuilder xmlDocumentBuilder = XmlDocumentBuilder.newInstance();
        configBean = xmlDocumentBuilder.parse(path);
        CrawlerConfig config = new CrawlerConfig();
        config.setConfigBean(configBean);
        return config;
    }

    public XmlConfigBean getConfigBean() {
        return configBean;
    }

    public void setConfigBean(XmlConfigBean configBean) {
        CrawlerConfig.configBean = configBean;
    }

    public enum CrawlerMode {
        LOCAL_MODE(0), DISTRIBUTED_MODE(1);
        private int val;

        CrawlerMode(int val) {
            this.val = val;
        }

        public static CrawlerMode paserMode(int value) {
            switch (value) {
                case 1:
                    return DISTRIBUTED_MODE;
                default:
                    return LOCAL_MODE;
            }
        }

        public int getValue() {
            return this.val;
        }

        public boolean equals(CrawlerMode crawlerMode) {
            return this.val == crawlerMode.val;
        }
    }

    public enum URL_STORE_MODE {
        LOCAL("LOCAL"), REDIS("REDIS");
        private String value;

        URL_STORE_MODE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public boolean equals(URL_STORE_MODE mode) {
            return this.value.equals(mode.getValue());
        }
    }
}

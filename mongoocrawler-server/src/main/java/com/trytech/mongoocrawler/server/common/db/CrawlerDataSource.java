package com.trytech.mongoocrawler.server.common.db;

import com.trytech.mongoocrawler.server.common.exception.DataSourceInitException;

import java.util.Map;

/**
 * Created by coliza on 2017/6/26.
 */
public abstract class CrawlerDataSource {
    private String name;
    private Class cls;

    protected Map<String,Object> propertiesMap;

    /*****************************
     * 初始化函数
     *****************************/
    public abstract void init() throws DataSourceInitException;
    /*****************************
     * 销毁函数
     *****************************/
    public abstract void destory();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getCls() {
        return cls;
    }

    public void setCls(Class cls) {
        this.cls = cls;
    }

    public Map<String, Object> getPropertiesMap() {
        return propertiesMap;
    }

    public void setPropertiesMap(Map<String, Object> propertiesMap) {
        this.propertiesMap = propertiesMap;
    }
}

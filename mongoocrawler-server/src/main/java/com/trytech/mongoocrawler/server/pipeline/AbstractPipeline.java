package com.trytech.mongoocrawler.server.pipeline;

import com.trytech.mongoocrawler.server.common.db.CrawlerDataSource;

import java.sql.SQLException;

/**
 * Created by coliza on 2017/6/15.
 */
public abstract class AbstractPipeline<T> {
    protected final static String DATASOURCE_NAME = "mysql";
    public abstract void store(T t) throws SQLException, ClassNotFoundException;

    public abstract CrawlerDataSource getDataSource();
}

package com.trytech.mongoocrawler.server.transport;

import com.trytech.mongoocrawler.server.parser.HtmlParser;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by coliza on 2017/6/18.
 */
public class UrlParserPair {
    private String url;
    private HtmlParser htmlParser;

    public UrlParserPair(String url){
        this(url, null);
    }
    public UrlParserPair(String url, HtmlParser htmlParser){
        this.url = url;
        this.htmlParser = htmlParser;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HtmlParser getHtmlParser() {
        return htmlParser;
    }

    public void setHtmlParser(HtmlParser htmlParser) {
        this.htmlParser = htmlParser;
    }

    public URL toURL() throws MalformedURLException {
        return new URL(url);
    }
}

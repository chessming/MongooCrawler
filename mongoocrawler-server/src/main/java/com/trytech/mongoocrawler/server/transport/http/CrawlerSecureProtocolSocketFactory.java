package com.trytech.mongoocrawler.server.transport.http;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created by coliza on 2017/3/17.
 */
public class CrawlerSecureProtocolSocketFactory  {
    private static class TrustAnyTrustManager implements X509TrustManager{
        public void checkClientTrusted(X509Certificate[] chain,String authType) throws CertificateException{

        }
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException{

        }
        public X509Certificate[] getAcceptedIssuers(){
            return new X509Certificate[]{};
        }
    }
    private static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session){
            return true;
        }
    }
}

package com.trytech.mongoocrawler.server.xml;

/**
 * Created by coliza on 2017/6/5.
 */
public abstract class CacheXmlConfigBean {
    public static enum CacheType{
        REDIS("REDIS");
        private String value;

        CacheType(String value){
            this.value = value;
        }
        public boolean equals(CacheType cacheType){
            if(this.value.equals(cacheType.value)){
                return true;
            }
            return false;
        }
        public static CacheType getCacheType(String cacheType){
            if(cacheType.equals(REDIS.value.toLowerCase())){
                return REDIS;
            }
            return null;
        }
    }
    private String ip;
    private int port;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public static CacheXmlConfigBean getCacheBean(CacheType cacheType){
        if(CacheType.REDIS.equals(cacheType)){
            return new RedisCacheXmlConfigBean();
        }
        return null;
    }
}

package com.trytech.mongoocrawler.server.xml;

import com.trytech.mongoocrawler.server.CrawlerConfig;
import lombok.Data;

/**
 * Created by coliza on 2018/5/24.
 */
@Data
public class ModeConfigBean {
    private CrawlerConfig.CrawlerMode crawlerMode;
    private int serverPort;
}

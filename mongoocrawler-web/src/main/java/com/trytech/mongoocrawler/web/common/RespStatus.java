package com.trytech.mongoocrawler.web.common;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月15日
 */
public enum RespStatus {
    OK(200, "成功"),
    CRAWLER_SERVICE_ERROR(1000, "爬虫服务异常");
    private int code;
    private String msg;

    RespStatus(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

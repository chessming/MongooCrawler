package com.trytech.mongoocrawler.web.vo;

import com.trytech.mongoocrawler.web.common.RespStatus;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月15日
 */
public class CommResp {
    //状态
    private RespStatus status;

    public static CommResp resp(RespStatus status) {
        CommResp resp = OK();
        resp.setStatus(status);
        return resp;
    }

    public static CommResp OK() {
        CommResp resp = new CommResp();
        resp.setStatus(RespStatus.OK);
        return resp;
    }

    public RespStatus getStatus() {
        return status;
    }

    public void setStatus(RespStatus status) {
        this.status = status;
    }
}
